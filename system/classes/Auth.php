<?php namespace App;


/**
 * Class auth authenticates user and permits to check if the user has been logged in
 * Automatically loaded when the controller has $requires_auth property.
 */
class Auth
{

    public $logged_in = FALSE;
    private $controller;
    private $action;

    function __construct($app)
    {
        $this->controller = $app->controller;
        $this->action = $app->action;
        if (isset($_SESSION['user_id'])) {
            $this->logged_in = TRUE;
            $user = get_first("SELECT *
                               FROM users
                               WHERE user_id = '{$_SESSION['user_id']}'");
            $this->load_user_data($user);

        }
    }

    /**
     * Dynamically add all user table fields as object properties to auth object
     * @param $user
     */
    public
    function load_user_data($user)
    {


        foreach ($user as $user_attr => $value) {
            $this->$user_attr = $value;
        }
        $this->logged_in = TRUE;
    }

    /**
     * Verifies if the user is logged in and authenticates if not and POST contains username, else displays the login form
     * @return bool Returns true when the user has been logged in
     */

    function require_auth()
    {
        global $db;


        // If user has already logged in...
        if ($this->logged_in) {
            return TRUE;
        }
        $this->admin_login = !empty($_POST['name']) && empty($_POST['password']) && User::isAdmin($_POST['name']);

        // Not all credentials were provided
        if (!(isset($_POST['name']))) {

            $this->show_login();

        }

        if (!empty($_POST['name']) && empty($_POST['password']) && User::isAdmin($_POST['name'])) {

            $this->show_login();

        }


        // Attempt to retrieve user data from database
        $name = addslashes($_POST['name']);
        $user = get_first("SELECT * 
                           FROM users LEFT JOIN counties USING (county_id)
                           WHERE name = '$name'
                           AND deleted = 0");

        // No such user or wrong password
        if (empty($user['user_id'])) {
            $this->show_login([__("Check the name and try again")]);
        }
        if ($user['is_admin'] && !password_verify($_POST['password'], $user['password'])) {
            $this->show_login([__("Wrong name or password")]);
        }

        //if the user was regular user and there is no ongoing event for this county then show login with error message
        if (!$user['is_admin'] && !Event::onGoing($user)) {
            $this->show_login([$user['county_id']?
                __('Sorry, neither you nor') . " $user[county_name] " . __('county has no ongoing events'):
                __('Sorry, you do not have any ongoing events')

            ]);
        }

        // User has provided correct login data if we are here
        User::login($user['user_id']);

        //Redirect admin to admin/
        if ($this->controller == DEFAULT_CONTROLLER && $this->action == 'index') {
            if ($user['is_admin']) {
                header('Location: ' . BASE_URL . 'admin/');
                exit();
            }
        }

        // Load $this->auth with users table's field values
        $this->load_user_data($user);


        return true;

    }

    /**
     * @param $errors
     */
    protected function show_login($errors = null)
    {
        // Display the login form
        require 'templates/auth_template.php';

        // Prevent loading the requested controller (not authenticated)
        exit();
    }


}
