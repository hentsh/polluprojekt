<?php namespace App;

use DateTime;

class admin extends Controller
{
    public $requires_auth = true;
    public $requires_admin = true;
    public $template = 'admin';
    public $selected_county_id;
    public $event_id;
    public $event_end;
    public $event_start;
    public $event_name;
    public $county_id;
    public $user_id;
    public $user_name;
    public $participants;
    public $county_name;

    public function __construct()
    {
        $this->selected_county_id = !isset($_GET['county_id']) || !is_numeric($_GET['county_id'])
            ? (empty($_SESSION['county_id']) ? 0 : $_SESSION['county_id']) : $_GET['county_id'];
        $this->selected_event_id = !isset($_GET['event_id']) || !is_numeric($_GET['event_id'])
            ? (empty($_SESSION['event_id']) ? 0 : $_SESSION['event_id']) : $_GET['event_id'];
        $this->county_id = !isset($_POST['county_id']) || !is_numeric($_POST['county_id']) ? false : $_POST['county_id'];
        $this->event_id = empty($_POST['event_id']) || !is_numeric($_POST['event_id']) ? false : $_POST['event_id'];
        $this->user_id = empty($_POST['user_id']) || !is_numeric($_POST['user_id']) ? false : $_POST['user_id'];
        $this->user_name = empty($_POST['name']) ? false : $_POST['name'];
        $this->county_name = empty($_POST['county_name']) ? false : $_POST['county_name'];
        $this->participants = empty($_POST['participants']) ? false : $_POST['participants'];
        $this->event_name = empty($_POST['event_name']) ? false : $_POST['event_name'];
        $this->event_start = empty($_POST['event_start']) ? false : $_POST['event_start'];
        $this->event_end = empty($_POST['event_end']) ? false : $_POST['event_end'];
        $_SESSION['county_id'] = $this->selected_county_id;
    }

    function index()
    {
        header('Location: ' . BASE_URL . 'admin/users');
        exit();
    }

    function users()
    {
        $this->users = User::get($this->selected_county_id);
        $this->counties = County::get();

    }

    function events()
    {
        $this->events = Event::get($this->selected_county_id ? [
            "event_id IN (SELECT event_id FROM participants WHERE participant_type_id = ".PARTICIPANT_TYPE_COUNTY." AND participant_id = $this->selected_county_id)"
        ] : null);
        $this->counties = County::get();
        $this->selected_county = $this->selected_county_id ? County::get($this->selected_county_id, true) : [];
        $this->participants = json_encode(Participant::get());
        $this->county_name = empty($this->counties[$this->selected_county_id]) ? '' : $this->counties[$this->selected_county_id]['county_name'];
        $this->current_year = date('Y');
    }

    function new_user()
    {
        if ($this->county_id === false || !$this->user_name) {
            stop(400, __('Invalid argument(s)'));
        }
        if (get_one("SELECT user_id FROM users WHERE deleted=0 AND name = '" . addslashes($this->user_name) . "'")) {
            stop(409, __('User already exists'));
        }

        // Set county to null if no county was selected
        $this->county_id = empty($this->county_id) ? null : $this->county_id;

        insert('users', [
            'county_id' => $this->county_id,
            'name' => $this->user_name,
        ]);

        stop(200);
    }

    function new_county()
    {
        if (!$this->county_name) {
            stop(400, __('Invalid argument(s)'));
        }
        if (get_one("SELECT county_id FROM counties WHERE county_deleted=0 AND county_name = '" . addslashes($this->county_name) . "'")) {
            stop(409, __('County already exists'));
        }

        insert('counties', [
            'county_name' => $this->county_name,
        ]);

        stop(200);
    }

    function new_event()
    {
        //Convert dates from text to objects
        $event_start = DateTime::createFromFormat('d.m.Y', $this->event_start);
        $event_end = DateTime::createFromFormat('d.m.Y', $this->event_end);

        if (!$this->event_name || !$event_start || !$event_end) {
            stop(400, __('Invalid argument(s)'));
        }

        $event_id = insert('events', [
            'event_name' => $this->event_name,
            'event_start' => $event_start->format('Y-m-d'),
            'event_end' => $event_end->format('Y-m-d'),
        ]);
        Event::replaceParticipants($event_id, $this->participants);
        stop(200);
    }

    function edit_user()
    {
        if (!$this->county_id || !$this->user_name || !$this->user_id) {
            stop(400, __('Invalid argument(s)'));
        }

        if (get_one("
            SELECT user_id FROM users 
            WHERE deleted = 0 
              AND user_id != $this->user_id 
              and name = '" . addslashes($this->user_name) . "'")) {
            stop(409, __('User already exists'));
        }

        update('users', [
            'county_id' => $this->county_id,
            'name' => $this->user_name,
        ], "user_id=$this->user_id");

        stop(200);
    }

    function edit_county()
    {
        if (!$this->county_id || !$this->county_name) {
            stop(400, __('Invalid argument(s)'));
        }

        $county_name = addslashes($this->county_name);

        if (get_one("
            SELECT county_name FROM counties 
            WHERE county_name = '$county_name' AND county_deleted = 0")) {
            stop(409, __('County already exists'));
        }

        update('counties', [
            'county_name' => $this->county_name,
        ], "county_id=$this->county_id");

        stop(200);
    }

    function edit_event()
    {
        //Convert dates from text to objects
        $event_start = DateTime::createFromFormat('d.m.Y', $this->event_start);
        $event_end = DateTime::createFromFormat('d.m.Y', $this->event_end);

        if (!$this->event_name || !$this->event_id || !$event_start || !$event_end) {
            stop(400, __('Invalid argument(s)'));
        }

        update('events', [
            'event_name' => $this->event_name,
            'event_start' => $event_start->format('Y-m-d'),
            'event_end' => $event_end->format('Y-m-d'),
        ], "event_id=$this->event_id");

        Event::replaceParticipants($this->event_id, $this->participants);

        stop(200);
    }

    function delete_user()
    {
        if (!$this->user_id) {
            stop(400, __('Invalid argument'));
        }

        if ($this->user_id == $this->auth->user_id) {
            stop(403, __('You cannot delete yourself'));
        }

        update('users', [
            'deleted' => 1
        ], "user_id=$this->user_id");

        stop(200);
    }

    function delete_county()
    {
        if (!$this->county_id) {
            stop(400, __('Invalid argument'));
        }

        // "Delete" county
        update('counties', [
            'county_deleted' => 1
        ], "county_id=$this->county_id");

        // "Delete" users in that county, except me
        $me = $this->auth->user_id;
        update('users', [
            'deleted' => 1
        ], "county_id=$this->county_id AND user_id != $me");

        // Set my county to NULL if it was the deleted county
        if ($this->auth->county_id == $this->county_id) {
            update('users', [
                'county_id' => null
            ], "user_id = $me");
        }

        stop(200);
    }

    function delete_event()
    {
        if (!$this->event_id) {
            stop(400, __('Invalid argument'));
        }

        update('events', [
            'event_deleted' => 1
        ], "event_id=$this->event_id");

        stop(200);
    }

    function import_users()
    {
        if (!empty($this->county_id) && !in_array($this->county_id, County::getValidIds())) {
            stop(400, __('Invalid county_id'));
        }
        $existing_users = User::import(
            $_FILES["xlsxFile"]["name"],
            $_FILES["xlsxFile"]["tmp_name"],
            $this->county_id);
        empty($existing_users) ?
            stop(200) :
            stop(409, __("Some users already existed: ") . implode(', ', $existing_users));
    }


    function logs()
    {
        $this->counties = County::get();
        $this->events = Event::get();
        $this->log = Activity::logs($this->selected_county_id, $this->selected_event_id);
    }

    function counties()
    {
        $this->counties = County::get();
    }


}