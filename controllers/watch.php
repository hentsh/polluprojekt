<?php namespace App;

class watch extends Controller
{
    function index()
    {
        $this->events = Event::getUserEvents($this->auth);

        // Redirect to first available event, if no event is given
        $random_event_id = count(array_keys($this->events)) ? array_keys($this->events)[0] : null;
        if ($random_event_id) {
            header('Location: ' . BASE_URL . 'watch/' . $random_event_id);
            exit();
        }
    }

    function view()
    {
        $this->events = Event::getUserEvents($this->auth);
        $this->current_event_id = $this->params[0];
    }

}