<?php

use App\User;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <title><?= PROJECT_NAME ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css?<?=COMMIT_HASH?>">
    <script src="vendor/components/jquery/jquery.min.js?<?=COMMIT_HASH?>"></script>
    <script src="vendor/components/bootstrap/js/bootstrap.min.js?<?=COMMIT_HASH?>"></script>
    <?php include 'templates/partials/favicon.php'?>

    <style>
        body {
            padding-top: 50px;
        }

        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
            text-align: center;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .modal-input input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .modal-input input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        span.input-group-addon {
            width: 50px;
        }

        div.input-group {
            width: 100%;
        }

        form.form-signin {
            background-color: #ffffff;
        }
    </style>
</head>

<body>

<div class="container">

    <form class="form-signin" method="post">
        <h2 class="form-signin-heading"><?= __('Login') ?></h2>
        <?php if (isset($errors)) {
            foreach ($errors as $error): ?>
                <div class="alert alert-danger">
                    <?= $error ?>
                </div>
            <?php endforeach;
        } ?>
        <label for="name"><?= __('Insert your name and surname') ?>:</label>

        <div class="input-group">
            <input id="name" value="<?= empty($_POST['name']) ? false : $_POST['name']?>" name="name" type="text" class="form-control" <?= $this->admin_login?'':'autofocus'?>>
        </div>

        <br/>
        <?php if ($this->admin_login) : ?>
        <label for="pass"><?= __('Your admin password') ?></label>

        <div class="input-group">
            <input id="pass" name="password" type="password" class="form-control" autofocus>
        </div>

        <br/>
        <?php endif; ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?= __('Sign in') ?></button>
    </form>

</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
