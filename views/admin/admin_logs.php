<style>
    .pause, .leave {
        background-color: pink;
    }
    .play, .timeupdate{
        background-color: lightgreen;
    }
    body > div.container > a:not(.active) {
        background-color: white !important;
        color: black !important;
        border: 0;
    }
    body > div.container > a.active {

    }
    nav#events > a.nav-link.active {
        background-color: #b352ff;
        border-radius: 5px;
        color: white;
</style>
<nav class="nav text-center">
    <a class="px-2 nav-link mx-auto <?= $selected_county_id == 0 ? 'active' : '' ?>"
       href="admin/logs?county_id=0"><?= __('All') ?></a>
    <?php foreach ($counties as $county): ?>
        <a class="px-2 nav-link mx-auto <?= $county['county_id'] == $selected_county_id ? 'active' : '' ?>"
           href="admin/logs?county_id=<?= $county['county_id'] ?>"><?= $county['county_name'] ?></a>
    <?php endforeach ?>
</nav>
<nav id="events" class="nav text-center">
    <a class="px-2 nav-link mx-auto <?= $selected_event_id == 0 ? 'active' : '' ?>"
       href="admin/logs?event_id=0"><?= __('All') ?></a>
    <?php foreach ($events as $event_id => $event): ?>
        <a class="px-2 nav-link mx-auto <?= $event_id == $selected_event_id ? 'active' : '' ?>"
           href="admin/logs?event_id=<?= $event_id ?>"><?= $event['event_name'] ?></a>
    <?php endforeach ?>
</nav>

<br>
<table class="table table table-nonfluid table-bordered table-hover pria-log bordered">
    <tr>
        <th><?= __('Time') ?></th>
        <th><?= __('User') ?></th>
        <th><?= __('Activity') ?></th>
    </tr>
    <?php foreach ($log as $row): ?>
        <tr class="<?=$row['activity_name']?>">
            <td><?=$row['activity_log_timestamp']?></td>
            <td><?=$row['name']?></td>
            <td><?=__($row['activity_description'])?></td>
        </tr>
    <?php endforeach ?>
</table>