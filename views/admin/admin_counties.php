<script src="https://use.fontawesome.com/d37013578f.js"></script>

<style>
    .form-import .file-import {
        display: none;
    }

    .input-box-place {
        padding-bottom: -1px !important;
        margin-bottom: -1px !important;
    }

    .form-group {
        padding: 1px;
        margin: 1px;
    }

    #input-new-county-name {
        width: 100%;
    }

    #btn-add {
        width: 100%;
    }

    .input-group-append {
        width: 100%;
        padding: 1px !important;
        margin: 1px !important;
    }

    body > div.container > div.input-group.mb-3.new-event-div > div > div:nth-child(1) > div.col-sm-12.col-md-11.my-0.px-md-0.input-box-place {
        margin-top: 2px !important;
    }

    body > div.container > div.input-group.mb-3.new-event-div > div > div:nth-child(1) > div.col-sm-12.col-md-1.px-md-0 {
        padding: 1px !important;
    }


</style>


<br>
<div class="input-group mb-3 new-event-div">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-11 my-0 px-md-0 input-box-place">
                <input type="text" class="form-control" id="input-new-county-name"
                       placeholder="<?= __("New county name") ?>" aria-label="New county name"
                       aria-describedby="basic-addon2">
            </div>

            <div class="col-sm-12 col-md-1 px-md-0">
                <div class="input-group-append justify-content-center d-md-table mx-auto form-group">
                    <button class="btn btn-success" id="btn-add"><?= __('Add') ?></button>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-12 col-md-12 px-md-2 text-muted">
                <sup><?= __('Name') ?></sup>
            </div>

        </div>
    </div>
</div>

<?php if (!empty($counties)): ?>
    <table class="table table table-nonfluid table-bordered table-hover table-counties bordered">
        <tr>
            <th><?= __('County') ?></th>
            <th></th>
        </tr>
        <?php foreach ($counties as $county): ?>
            <tr data-county_id="<?= $county['county_id'] ?>">
                <td><?= $county['county_name'] ?></td>
                <td>
                    <a class="edit" data-toggle="modal" data-target=".modal"
                       href="counties/edit/<?= $county['county_id'] ?>"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
                    <a class="delete" href="counties/delete/<?= $county['county_id'] ?>"><i
                                class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
<?php endif ?>

<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Edit county') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="mr-sm-2" for="input-county-new-name"><?= __('Name') ?></label>
                    <input value="" type="text" class="form-control" id="input-county-new-name"
                           placeholder="<?= __("County's new name") ?>" aria-label="County's new name"
                           aria-describedby="basic-addon2">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save"><?= __('Save changes') ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    var selectedCountyTr;

    $('#btn-add').click(function () {
        ajax('admin/new_county', {
            county_name: $('#input-new-county-name').val()
        }, RELOAD)
    });

    $('.edit').click(function (e) {
        selectedCountyTr = $(this).closest('tr')
        // Prevent from navigating away from the page
        e.preventDefault()

        // Fill modal text field with selected county name
        $('#input-county-new-name').val(selectedCountyTr.find('td:nth-child(1)').html())

        // Set modal dropdown value to selected county's county
        $('#county-id').val(selectedCountyTr.data('county_id'))

    });

    $('.delete').click(function (e) {
        selectedCountyTr = $(this).closest('tr')
        // Prevent from navigating away from the page
        e.preventDefault()

        //Send delete command to server, if county confirms
        if (confirm('<?=__('WARNING: THIS WILL DELETE THE COUNTY AND EVERY USER IN THE COUNTY!');?>')) {
            ajax('admin/delete_county', {
                county_id: selectedCountyTr.data('county_id')
            }, RELOAD)
        }

    });

    $('.btn-save').click(function () {
        ajax('admin/edit_county', {
            county_name: $('#input-county-new-name').val(),
            county_id: selectedCountyTr.data('county_id'),
        }, RELOAD)
    });

</script>