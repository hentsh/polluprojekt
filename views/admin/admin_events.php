<?php use App\Event;?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="https://use.fontawesome.com/d37013578f.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"/>
<script src="vendor/components/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="vendor/components/jqueryui/themes/base/jquery-ui.css">
<script type="text/javascript" src="assets/typeahead/js/typeahead.bundle.min.js"></script>
<link rel="stylesheet" href="assets/typeahead/css/tokenfield-typeahead.css">
<link rel="stylesheet" href="assets/bootstrap-tokenfield/css/bootstrap-tokenfield.css">
<script src="assets/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>

<nav class="nav text-center">
    <a class="px-2 nav-link mx-auto <?= $selected_county_id == 0 ? 'active' : '' ?>"
       href="admin/events?county_id=0"><?= __('All') ?></a>
    <?php foreach ($counties as $county): ?>
        <a class="px-2 nav-link mx-auto <?= $county['county_id'] == $selected_county_id ? 'active' : '' ?>"
           href="admin/events?county_id=<?= $county['county_id'] ?>"><?= $county['county_name'] ?></a>
    <?php endforeach ?>
</nav>
<br>
<style>
    .form-group {
        padding: 1px;
        margin: 1px;
    }

    body > div.container > div.container > form > div {
        width: 100%;
        margin: 0;
        padding: 0;
    }

    #new-participants, #datetimepicker1, #datetimepicker2, #btn-add, #new-name {
        width: 100%;
    }

    /* Fix modal's tokenfield showing its dropdown under the modal */
    .ui-autocomplete { z-index:2147483647; }
</style>
<form class="form-inline new-event-div">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12 col-md-3 px-md-0">
                <div class="form-group">
                    <input type="text" class="form-control" id="new-name"
                           value="<?= "$county_name infopäev $current_year" ?>" aria-label="New event's name"
                           aria-describedby="basic-addon2">
                </div>
            </div>
            <div class="col-sm-12 col-md-4 px-md-0">
                <div class="form-group">
                    <input type="text" class="form-control token-field" id="new-participants" value=""/>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 px-md-0">
                <div class="form-group">
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">

                        <input type="text" class="form-control datetimepicker-input" id="new-start"
                               placeholder="<?= __('Begin date') ?>"
                               data-target="#datetimepicker1"/>
                        <div class="input-group-append" data-target="#datetimepicker1"
                             data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 px-md-0">
                <div class="form-group">
                    <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="new-end"
                               placeholder="<?= __('End date') ?>"
                               data-target="#datetimepicker2"/>
                        <div class="input-group-append" data-target="#datetimepicker2"
                             data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-1 px-md-0">
                <div class="form-group">
                    <a href="#" id="btn-add" class="btn btn-success d-flex justify-content-center d-md-table mx-auto">
                        <?= __('Add') ?></a>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-12 col-md-3 px-md-0 text-center text-muted">
                <sup><?= __('Event name') ?></sup>
            </div>
            <div class="col-sm-12 col-md-4 px-md-0 text-center text-muted">
                <sup><?= __('Participants') ?></sup>
            </div>
            <div class="col-sm-12 col-md-2 px-md-0 text-center text-muted">
                <sup><?= __('Begin date') ?></sup>
            </div>
            <div class="col-sm-12 col-md-2 px-md-0 text-center text-muted">
                <sup><?= __('End date') ?></sup>
            </div>
            <div class="col-sm-12 col-md-1 px-md-0 text-center text-muted">
                <sup></sup>
            </div>
        </div>
    </div>

</form>

<br>

<?php if (!empty($events)): ?>
    <div class="row justify-content-center">
        <div class="col align-self-center">
            <div>
                <table class="table table-nonfluid table-bordered table-hover table-events bordered">
                    <tr>
                        <th><?= __('Name') ?></th>
                        <th><?= __('Begins') ?></th>
                        <th><?= __('Ends') ?></th>
                        <th><?= __('Participants') ?></th>
                        <th></th>
                    </tr>
                    <?php foreach ($events as $event_id => $event): ?>
                        <tr data-event_id="<?= $event_id ?>" data-participants="<?= base64_encode(Event::convert($event['participants'])) ?>">
                            <td><?= $event['event_name'] ?></td>
                            <td><?= $event['event_start'] ?></td>
                            <td><?= $event['event_end'] ?></td>
                            <td><?php foreach ($event['participants'] as $participant): ?>
                                    <?php if ($participant['participant_id']): ?>
                                        <span class="badge badge-<?= substr($participant['participant_name'], 0, 1) == '@' ? 'primary' : 'secondary' ?>">
                                        <?= $participant['participant_name'] ?>
                                    </span>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <a class="edit" data-toggle="modal" data-target=".modal"
                                   href="events/edit/<?= $event['event_id'] ?>"><i
                                            class="fa fa-pencil-square-o"></i></a>&nbsp;
                                <a class="delete"
                                   href="events/delete/<?= $event['event_id'] ?>"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Edit name') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="mr-sm-2" for="modal-name"><?= __('Name') ?></label>
                    <input value="" type="text" class="form-control" id="modal-name"
                           placeholder="<?= __("Event's new name") ?>" aria-label="New event's name"
                           aria-describedby="basic-addon2">
                </div>

                <div class="form-group">
                    <label class="mr-sm-2" for="modal-start"><?= __('Begins') ?></label>
                    <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="modal-start"
                               data-target="#datetimepicker3"/>
                        <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="mr-sm-2" for="modal-end"><?= __('Ends') ?></label>
                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="modal-end"
                               data-target="#datetimepicker4"/>
                        <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="mr-sm-2" for="modal-participants"><?= __('Participants') ?></label>
                    <input type="text" class="form-control token-field" id="modal-participants" value=""/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save"><?= __('Save changes') ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    var selectedUserTr;
    var availableTokens = <?= $participants ?>;

    $('#btn-add').click(function (e) {

        // Prevent from navigating away from the page
        e.preventDefault()
        ajax('admin/new_event', {
            participants: $('#new-participants').tokenfield('getTokensList'),
            event_name: $('#new-name').val(),
            event_start: $('#new-start').val(),
            event_end: $('#new-end').val()
        }, function (json) {
            location.href = 'admin/events?county_id=' + $('#new-county-id').val()
        })
    });

    $('.edit').click(function (e) {
        selectedUserTr = $(this).closest('tr')

        // Prevent from navigating away from the page
        e.preventDefault()

        // Fill modal text field with selected event name
        $('#modal-name').val(selectedUserTr.find('td:nth-child(1)').html())

        // Fill modal text field with selected event dates
        $('#modal-start').val(selectedUserTr.find('td:nth-child(2)').html())
        $('#modal-end').val(selectedUserTr.find('td:nth-child(3)').html())

        // Set modal tokenfield value to selected event's participants
        console.log(atob(selectedUserTr.data('participants')))
        initTokenfield($('#modal-participants'))
        $('#modal-participants').tokenfield('setTokens', JSON.parse(atob(selectedUserTr.data('participants'))));


    });

    $('.delete').click(function (e) {
        selectedUserTr = $(this).closest('tr')

        // Prevent from navigating away from the page
        e.preventDefault()

        //Send delete command to server, if event confirms
        if (confirm('<?=__('Are you sure?');?>')) {
            ajax('admin/delete_event', {
                event_id: selectedUserTr.data('event_id')
            }, RELOAD)
        }

    });

    $('.btn-save').click(function () {
        ajax('admin/edit_event', {
            event_name: $('#modal-name').val(),
            event_id: selectedUserTr.data('event_id'),
            participants: $('#modal-participants').tokenfield('getTokensList'),
            event_start: $('#modal-start').val(),
            event_end: $('#modal-end').val()
        }, function () {
            location.href = 'admin/events?county_id=' + $('#modal-county-id').val()
        })
    });

    $(function () {
        $('.date').each(function (index, element) {
            $(element).datetimepicker({
                locale: 'et',
                format: 'L',
                allowInputToggle: true
            });
        })

        $('.token-field').each(function (index, element) {
            initTokenfield(element)
        })
        // Default participants for the new event form is selected county users
        $('#new-participants').tokenfield('setTokens', <?= $selected_county ?>);
    });

    function initTokenfield(element) {
        console.log('eee')
        $(element).tokenfield({
            autocomplete: {
                source: availableTokens,
                delay: 100
            },
            showAutocompleteOnFocus: true,
        });

        $(element).on('tokenfield:createtoken', function (event) {

            var exists = true;
            var existingTokens = $(this).tokenfield('getTokens');

            // Prevent duplicates
            $.each(existingTokens, function (index, token) {
                if (token.value === event.attrs.value)
                    event.preventDefault();
            });

            // Allow only available tokens
            $.each(availableTokens, function (index, token) {
                if (token.value === event.attrs.value)
                    exists = false;
            });

            if (exists === true)
                event.preventDefault();

        });
    }

</script>