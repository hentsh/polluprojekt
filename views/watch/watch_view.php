<style>
    video {
        /* override other styles to make responsive */
        width: 100% !important;
        height: auto !important;
    }

    body {
        background-color: black;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
<!-- Or if you want a more recent canary version -->
<!-- <script src="https://cdn.jsdelivr.net/npm/hls.js@canary"></script> -->

<?php if (count($events) > 1): ?>
<ul class="nav nav-tabs">
    <?php foreach ($events as $event_id => $event): ?>
        <li class="nav-item">
            <a class="nav-link <?= $event_id == $current_event_id ? 'active' : '' ?>"
               href="watch/<?= $event_id ?>"><?= $event['event_name'] ?></a>
        </li>
    <?php endforeach; ?>
</ul>
<?php endif ?>


<video id="video" controls></video>

<script>

    var vid = document.getElementById("video");
    var lastTimeUpdate = null;
    vid.onabort = function () {
        ajax('activities/create/abort/', {event_id: <?= $current_event_id ?>})
    };
    //vid.oncanplay = function () {ajax('activities/create/canplay/',{event_id: <?= $current_event_id ?>})};
    //vid.oncanplaythrough = function () {ajax('activities/create/canplaythrough/',{event_id: <?= $current_event_id ?>})};
    //vid.ondurationchange = function () {ajax('activities/create/durationchange/',{event_id: <?= $current_event_id ?>})};
    vid.onemptied = function () {
        ajax('activities/create/emptied/', {event_id: <?= $current_event_id ?>})
    };
    vid.onended = function () {
        ajax('activities/create/ended/', {event_id: <?= $current_event_id ?>})
    };
    vid.onerror = function () {
        ajax('activities/create/error/', {event_id: <?= $current_event_id ?>})
    };
    //vid.onloadeddata = function () {ajax('activities/create/loadeddata/',{event_id: <?= $current_event_id ?>})};
    //vid.onloadedmetadata = function () {ajax('activities/create/loadedmetadata/',{event_id: <?= $current_event_id ?>})};
    //vid.onloadstart = function () {ajax('activities/create/loadstart/',{event_id: <?= $current_event_id ?>})};
    vid.onpause = function () {
        ajax('activities/create/pause/', {event_id: <?= $current_event_id ?>})
    };
    vid.onplay = function () {
        ajax('activities/create/play/', {event_id: <?= $current_event_id ?>})
    };
    //vid.onplaying = function () {ajax('activities/create/playing/',{event_id: <?= $current_event_id ?>})};
    //vid.onprogress = function () {ajax('activities/create/progress/',{event_id: <?= $current_event_id ?>})};
    vid.onratechange = function () {
        ajax('activities/create/ratechange/', {event_id: <?= $current_event_id ?>})
    };
    vid.onseeked = function () {
        ajax('activities/create/seeked/', {event_id: <?= $current_event_id ?>})
    };
    //vid.onseeking = function () {ajax('activities/create/seeking/',{event_id: <?= $current_event_id ?>})};
    vid.onstalled = function () {
        ajax('activities/create/stalled/', {event_id: <?= $current_event_id ?>})
    };
    vid.onsuspend = function () {
        ajax('activities/create/suspend/', {event_id: <?= $current_event_id ?>})
    };
    vid.ontimeupdate = function () {
        dateNow = Date.now();
        if ((dateNow - (60 * 1000)) > lastTimeUpdate) {
            ajax('activities/create/timeupdate/', {event_id: <?= $current_event_id ?>}, function (response) {
                lastTimeUpdate = dateNow;
            })
        }
    }
    vid.onvolumechange = function () {
        ajax('activities/create/volumechange/', {event_id: <?= $current_event_id ?>})
    };
    //vid.onwaiting = function () {ajax('activities/create/waiting/',{event_id: <?= $current_event_id ?>})};

    var video = document.getElementById('video');
    if (Hls.isSupported()) {
        var hls = new Hls();
        hls.loadSource('<?=BASE_URL?>videos/<?=$current_event_id?>/video.m3u8');
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function () {
            video.play();
        });
    }
        // hls.js is not supported on platforms that do not have Media Source Extensions (MSE) enabled.
        // When the browser has built-in HLS support (check using `canPlayType`), we can provide an HLS manifest (i.e. .m3u8 URL) directly to the video element through the `src` property.
        // This is using the built-in support of the plain video element, without using hls.js.
        // Note: it would be more normal to wait on the 'canplay' event below however on Safari (where you are most likely to find built-in HLS support) the video.src URL must be on the user-driven
    // white-list before a 'canplay' event will be emitted; the last video event that can be reliably listened-for when the URL is not on the white-list is 'loadedmetadata'.
    else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = '<?=BASE_URL?>videos/<?=$current_event_id?>/video.m3u8';
        video.addEventListener('loadedmetadata', function () {
            video.play();
        });
    }

    $(window).on("beforeunload", function () {
        ajax('activities/create/leave');
    })
</script>