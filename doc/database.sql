-- MySQL dump 10.13  Distrib 5.7.26, for osx10.10 (x86_64)
--
-- Host: localhost    Database: polluprojekt
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `activity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Autocreated',
  `activity_name` varchar(50) NOT NULL COMMENT 'Autocreated',
  `activity_description` varchar(191) NOT NULL,
  `activity_description_original` varchar(191) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'abort','video loading aborted','the loading of an audio/video is aborted'),(2,'canplay','the browser is ready to start playing the video','the browser can start playing the audio/video'),(3,'canplaythrough','the browser is ready to play the video without stopping the caching','the browser can play through the audio/video without stopping for buffering'),(4,'durationchange','video duration changed','the duration of the audio/video is changed'),(5,'emptied','the current playlist is empty','the current playlist is empty'),(6,'ended','watched the video to the end','the current playlist is ended'),(7,'error','there was an error loading the video','an error occurred during the loading of an audio/video'),(8,'loadeddata','the browser has loaded the current video frame','the browser has loaded the current frame of the audio/video'),(9,'loadedmetadata','the browser has loaded video metadata','the browser has loaded meta data for the audio/video'),(10,'loadstart','the browser searches for the video','the browser starts looking for the audio/video'),(11,'pause','paused the video','the audio/video has been paused'),(12,'play','started the video','the audio/video has been started or is no longer paused'),(13,'playing','the video is played after the buffering is paused or stopped','the audio/video is playing after having been paused or stopped for buffering'),(14,'progress','the browser downloads the video','the browser is downloading the audio/video'),(15,'ratechange','the video playback speed is changed','the playing speed of the audio/video is changed'),(16,'seeked','fast forwarded video','the user is finished moving/skipping to a new position in the audio/video'),(17,'seeking','the user starts moving / jumping to a new location in the video','the user starts moving/skipping to a new position in the audio/video'),(18,'stalled','the browser is trying to retrieve media data, but the data is not available','the browser is trying to get media data, but data is not available'),(19,'suspend','the browser does not intentionally receive media data','the browser is intentionally not getting media data'),(20,'timeupdate','watching a video','the current playback position has changed'),(21,'volumechange','the volume has been changed','the volume has been changed'),(22,'waiting','the video stops because it needs to buffer the next frame','the video stops because it needs to buffer the next frame'),(99,'login','logged in','login'),(100,'leave','left the site','leave'),(101,'logout','logged out','logout');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `activity_log_timestamp` datetime NOT NULL,
  `activity_log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Autocreated',
  `user_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL COMMENT 'Autocreated',
  `event_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`activity_log_id`),
  KEY `activity_log_events_event_id_fk` (`event_id`),
  KEY `activity_log_users_user_id_fk` (`user_id`),
  KEY `activity_log_activities_activity_id_fk` (`activity_id`),
  CONSTRAINT `activity_log_activities_activity_id_fk` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`),
  CONSTRAINT `activity_log_events_event_id_fk` FOREIGN KEY (`event_id`) REFERENCES `events` (`event_id`),
  CONSTRAINT `activity_log_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counties`
--

DROP TABLE IF EXISTS `counties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counties` (
    `county_id`      TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    `county_name`    VARCHAR(191)        NOT NULL,
    `county_deleted` TINYINT(4)          NOT NULL DEFAULT '0',
    PRIMARY KEY (`county_id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 16
    DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counties`
--

LOCK TABLES `counties` WRITE;
/*!40000 ALTER TABLE `counties`
    DISABLE KEYS */;
INSERT INTO `counties`
VALUES (1, 'Harju', 0),
    (2, 'Tartu', 0),
    (3, 'Ida-Viru', 0),
    (4, 'Pärnu', 0),
    (5, 'Lääne-Viru', 0),
    (6, 'Viljandi', 0),
    (7, 'Rapla', 0),
    (8, 'Võru', 0),
    (9, 'Saare', 0),
    (10, 'Jõgeva', 0),
    (11, 'Järva', 0),
    (12, 'Valga', 0),
    (13, 'Põlva', 0),
    (14, 'Lääne', 0),
    (15, 'Hiiu', 0);
/*!40000 ALTER TABLE `counties`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
    `event_id`      INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `event_name`    VARCHAR(191)     NOT NULL,
    `event_deleted` TINYINT(4)       NOT NULL DEFAULT '0',
    `event_start`   DATETIME         NOT NULL,
    `event_end`     DATETIME         NOT NULL,
    PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'asd',1,'2020-04-15 00:00:00','2020-04-15 00:00:00'),(2,'Harju infopäev 2020 asd',1,'2020-04-02 00:00:00','2020-04-24 00:00:00'),(3,'Hiiu infopäev 2020 asdasd',1,'2020-04-15 00:00:00','2020-04-15 00:00:00'),(4,'Hiiu infopäev 2020 asdasd',1,'2020-04-15 00:00:00','2020-04-15 00:00:00'),(5,'Rapla infopäev 2020 adssad',1,'2020-04-16 00:00:00','2020-04-16 00:00:00'),(6,'Harju infopäev 2020 asdasdsad',1,'2020-04-18 00:00:00','2020-05-02 00:00:00'),(7,'Harju infopäev 2020 asd',1,'2020-04-02 00:00:00','2020-04-24 00:00:00'),(8,'Harju infopäev 2020',0,'2020-04-19 00:00:00','2020-04-21 00:00:00'),(9,'Hiiu infopäev 2020',0,'2020-04-19 00:00:00','2020-04-21 00:00:00'),(10,'Ida-Viru infopäev 2020',0,'2020-04-20 00:00:00','2020-04-22 00:00:00'),(11,'Jõgeva infopäev 2020',0,'2020-04-20 00:00:00','2020-04-22 00:00:00'),(12,'Tartu infopäev 2020',0,'2020-04-21 00:00:00','2020-04-23 00:00:00'),(13,'Lääne infopäev 2020',0,'2020-04-23 00:00:00','2020-04-26 00:00:00'),(14,'Järva infopäev 2020',0,'2020-04-23 00:00:00','2020-04-26 00:00:00'),(15,'Pärnu infopäev 2020',0,'2020-04-26 00:00:00','2020-04-28 00:00:00'),(16,'Viljandi infopäev 2020',0,'2020-04-28 00:00:00','2020-04-29 00:00:00'),(17,'Saare infopäev 2020',0,'2020-04-28 00:00:00','2020-04-30 00:00:00'),(18,'Lääne-Viru infopäev 2020',0,'2020-04-29 00:00:00','2020-05-03 00:00:00'),(19,'Põlva infopäev 2020',0,'2020-05-03 00:00:00','2020-05-05 00:00:00'),(20,'Rapla infopäev 2020',0,'2020-05-04 00:00:00','2020-05-06 00:00:00'),(21,'Valga infopäev 2020',0,'2020-05-05 00:00:00','2020-05-07 00:00:00'),(22,'Võru infopäev 2020',0,'2020-05-05 00:00:00','2020-05-07 00:00:00'),(23,'KSM 2020',0,'2020-06-03 00:00:00','2020-07-01 00:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participants` (
    `participant_id` int(10) unsigned NOT NULL,
    `participant_type_id` tinyint(3) unsigned NOT NULL,
    `event_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`participant_id`,`participant_type_id`,`event_id`),
    KEY `participants_events_event_id_fk` (`event_id`),
    CONSTRAINT `participants_events_event_id_fk` FOREIGN KEY (`event_id`) REFERENCES `events`(`event_id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participants`
--

LOCK TABLES `participants` WRITE;
/*!40000 ALTER TABLE `participants`
    DISABLE KEYS */;
INSERT INTO `participants`
VALUES (1, 1, 8),
    (15, 1, 9),
    (3, 1, 10),
    (10, 1, 11),
    (2, 1, 12),
    (14, 1, 13),
    (11, 1, 14),
    (4, 1, 15),
    (6, 1, 16),
    (9, 1, 17),
    (5, 1, 18),
    (13, 1, 19),
    (7, 1, 20),
    (12, 1, 21),
    (8, 1, 22);
/*!40000 ALTER TABLE `participants`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
    `translation_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `phrase`         VARBINARY(765)   NOT NULL,
    `language`       CHAR(3)          NOT NULL,
    `translation`    VARCHAR(191) DEFAULT NULL,
    `controller`     VARCHAR(15)      NOT NULL,
    `action`         VARCHAR(20)      NOT NULL,
    PRIMARY KEY (`translation_id`),
    UNIQUE KEY `language_phrase_controller_action_index`(`language`, `phrase`, `controller`, `action`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 111
    DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations`
    DISABLE KEYS */;
INSERT INTO `translations`
VALUES (6, _binary 'Logout', 'et', 'Logi välja', 'global', 'global'),
    (7, _binary 'Settings', 'et', 'Seaded', 'global', 'global'),
(8, _binary 'Logged in as', 'et', 'Sisse logitud kui', 'global', 'global'),
    (11, _binary 'Log out', 'et', 'Logi välja', 'global', 'global'),
(12, _binary 'Server returned response in an unexpected format', 'et', 'Server saatis vastuse ootamatus vormingus',
 'global', 'global'),
    (13, _binary 'Forbidden', 'et', 'Keelatud', 'global', 'global'),
(14, _binary 'Server returned an error', 'et', 'Server tagastas vea', 'global', 'global'),
(15, _binary 'Please sign in', 'et', 'Palun logige sisse', 'global', 'global'),
    (16, _binary 'Email', 'et', 'E-post', 'global', 'global'),
    (17, _binary 'Password', 'et', 'Parool', 'global', 'global'),
    (18, _binary 'Sign in', 'et', 'Logi sisse', 'global', 'global'),
    (19, _binary 'Oops...', 'et', 'Vabandust...', 'global', 'global'),
    (20, _binary 'Close', 'et', 'Sulge', 'global', 'global'),
(21, _binary 'Server returned an error. Please try again later ', 'et',
 'Server tagastas vea. Palun proovi hiljem uuesti', 'global', 'global'),
    (26, _binary 'User', 'et', 'Kasutaja', 'global', 'global'),
(27, _binary 'Minutes watched', 'et', 'Minuteid vaadatud', 'global', 'global'),
    (28, _binary 'Edit', 'et', 'Muuda', 'global', 'global'),
    (29, _binary 'Delete', 'et', 'Kustuta', 'global', 'global'),
(32, _binary 'You\'re not admin!', 'et', 'Sa pole administraator!', 'global', 'global'),
(33, _binary 'Access denied', 'et', 'Ligipääs keelatud', 'global', 'global'),
(34, _binary 'Participant\'s name', 'et', 'Osaleja nimi', 'global', 'global'),
    (35, _binary 'Add', 'et', 'Lisa', 'global', 'global'),
(36, _binary 'New participant\'s name', 'et', 'Uue osaleja nimi', 'global', 'global'),
    (37, _binary 'All', 'et', 'Kõik', 'global', 'global'),
    (38, _binary 'Edit name', 'et', 'Muuda nime', 'global', 'global'),
(39, _binary 'Participant\'s new name', 'et', 'Osaleja uus nimi', 'global', 'global'),
    (40, _binary 'Name', 'et', 'Nimi', 'global', 'global'),
    (41, _binary 'County', 'et', 'Maakond', 'global', 'global'),
(42, _binary 'Are you sure?', 'et', 'Oled sa kindel?', 'global', 'global'),
(43, _binary 'You cannot delete yourself', 'et', 'Sa ei saa ennast kustutada', 'global', 'global'),
    (44, _binary 'Users', 'et', 'Kasutajad', 'global', 'global'),
    (45, _binary 'Events', 'et', 'Üritused', 'global', 'global'),
    (46, _binary 'Event name', 'et', 'Ürituse nimi', 'global', 'global'),
(47, _binary 'Event new name', 'et', 'Ürituse uus nimi', 'global', 'global'),
    (48, _binary 'Begins', 'et', 'Algab', 'global', 'global'),
    (49, _binary 'Ends', 'et', 'Lõpeb', 'global', 'global'),
    (50, _binary 'Start', 'et', 'Alusta', 'global', 'global'),
(51, _binary 'Event new start', 'et', 'Ürituse uus algus', 'global', 'global'),
    (52, _binary 'End', 'et', 'Lõpp', 'global', 'global'),
(53, _binary 'Event new end', 'et', 'Ürituse uus lõpp', 'global', 'global'),
    (54, _binary 'Begin date', 'et', 'Alguskuupäev', 'global', 'global'),
    (55, _binary 'End date', 'et', 'Lõppkuupäev', 'global', 'global'),
    (56, _binary 'Login', 'et', 'Logi sisse', 'global', 'global'),
(57, _binary 'Insert your name and surname', 'et', 'Sisestage oma ees- ja perekonnanimi', 'global', 'global'),
(58, _binary 'Your admin password', 'et', 'Sinu administraatori parool', 'global', 'global'),
    (59, _binary 'Import', 'et', 'Impordi', 'global', 'global'),
    (60, _binary 'Logs', 'et', 'Logid', 'global', 'global'),
    (61, _binary 'Time', 'et', 'Aeg', 'global', 'global'),
    (62, _binary 'Activity', 'et', 'Tegevus', 'global', 'global'),
(76, _binary 'started the video', 'et', 'alustas video vaatamist', 'global', 'global'),
    (80, _binary 'logged in', 'et', 'logis sisse', 'global', 'global'),
    (85, _binary 'Sorry', 'et', 'Vabandust', 'global', 'global'),
(86, _binary 'county has no ongoing events', 'et', 'maakonnal ei ole ühtegi käimasolevat üritust', 'global', 'global'),
    (87, _binary 'asd', 'et', '{untranslated}', 'global', 'global'),
(92, _binary 'Event\'s new name', 'et', 'Ürituse uus nimi', 'global', 'global'),
(93, _binary 'Invalid argument(s)', 'et', 'Midagi jäi täitmata', 'global', 'global'),
(94, _binary 'Some users already existed: ', 'et', 'Mõned kasutajad juba eksisteerisid: ', 'global', 'global'),
    (95, _binary 'Admin', 'et', 'Admin', 'global', 'global'),
(96, _binary 'Invalid .xlsx file', 'et', '.xlsx fail ei ole õiges formaadis', 'global', 'global'),
    (97, _binary 'Save changes', 'et', 'Salvesta', 'global', 'global'),
(98, _binary 'Wrong name or password', 'et', 'Vale nimi või parool', 'global', 'global'),
(99, _binary 'Check the name and try again', 'et', 'Kontrolli nime ja proovi uuesti', 'global', 'global'),
(100, _binary 'User already exists', 'et', 'Kasutaja on juba olemas', 'global', 'global'),
    (101, _binary 'None', 'et', 'Määramata', 'global', 'global'),
    (102, _binary 'Participants', 'et', 'Osalejad', 'global', 'global'),
    (105, _binary 'Counties', 'et', 'Maakonnad', 'global', 'global'),
(106, _binary 'New county name', 'et', 'Uue maakonna nimi', 'global', 'global'),
(107, _binary 'Edit county', 'et', 'Muuda maakonda', 'global', 'global'),
(108, _binary 'County\'s new name', 'et', 'Maakonna uus nimi', 'global', 'global'),
(109, _binary 'WARNING: THIS WILL DELETE THE COUNTY AND EVERY USER IN THE COUNTY!', 'et',
 'HOIATUS: see kustutab maakonna ja kõik kasutajad maakonnas!', 'global', 'global'),
(110, _binary 'County already exists', 'et', 'Maakond juba eksisteerib', 'global', 'global');
/*!40000 ALTER TABLE `translations`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
    `user_id`   INT(10) UNSIGNED    NOT NULL AUTO_INCREMENT,
    `is_admin`  TINYINT(4)          NOT NULL DEFAULT '0',
    `password`  VARCHAR(191)        NOT NULL DEFAULT '',
    `email`     VARCHAR(191)        NOT NULL DEFAULT '',
    `deleted`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    `name`      VARCHAR(191)        NOT NULL,
    `county_id` TINYINT(3) UNSIGNED          DEFAULT '0',
    PRIMARY KEY (`user_id`),
    KEY `users_counties_county_id_fk`(`county_id`),
    CONSTRAINT `users_counties_county_id_fk` FOREIGN KEY (`county_id`) REFERENCES `counties`(`county_id`)
) ENGINE=InnoDB AUTO_INCREMENT=731 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'$2y$10$w7zc4MQ91DhNPByb0MU6iOGAAgusMQdaimG0JEPBrhK6Nmf3NP85G','demo@example.com',0,'Jaan Sõrra',1),(2,0,'','',0,'Liia Allas',7),(3,0,'','',0,'Tiina Pabor',1),(4,0,'','',1,'Aare Papp',1),(5,0,'','',1,'Saima Patrael',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-07 22:33:09
