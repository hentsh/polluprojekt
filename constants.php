<?php

/**
 * Application's constants
 */

// Project constants
define('PROJECT_NAME', 'Veebikoolitused');
define('PROJECT_SESSION_ID', 'SESSID_INFOPAEVAD'); // For separating sessions of multiple Halo projects running on same server
define('DEFAULT_CONTROLLER', 'watch');
define('DEVELOPER_EMAIL', 'dev@example.com'); // Where to send errors
define('FACEBOOK_APP_ID', '1000000000000001'); // For FB login
define('FACEBOOK_SECRET', 'ffffffffffffffffffffffffffffffff'); // For FB login
define('FORCE_HTTPS', false); // Force HTTPS connections
define('GOOGLE_CLIENT_ID', '1000000000000-ffffffffffffffffffffffffffffffff.apps.googleusercontent.com'); // For G login
define('GOOGLE_CLIENT_SECRET', 'sssssssssssssssss-ss_SSS');
define('GOOGLE_REDIRECT_URI', 'login_google/callback'); // For G login
define('WEBSITE_LANGUAGES', 'et'); // Languages this website supports. The first one is the default

define('ACTIVITY_VIDEO_ABORT',1);
define('ACTIVITY_VIDEO_CANPLAY',2);
define('ACTIVITY_VIDEO_CANPLAYTHROUGH',3);
define('ACTIVITY_VIDEO_DURATIONCHANGE',4);
define('ACTIVITY_VIDEO_EMPTIED',5);
define('ACTIVITY_VIDEO_ENDED',6);
define('ACTIVITY_VIDEO_ERROR',7);
define('ACTIVITY_VIDEO_LOADEDDATA',8);
define('ACTIVITY_VIDEO_LOADEDMETADATA',9);
define('ACTIVITY_VIDEO_LOADSTART',10);
define('ACTIVITY_VIDEO_PAUSE',11);
define('ACTIVITY_VIDEO_PLAY',12);
define('ACTIVITY_VIDEO_PLAYING',13);
define('ACTIVITY_VIDEO_PROGRESS',14);
define('ACTIVITY_VIDEO_RATECHANGE',15);
define('ACTIVITY_VIDEO_SEEKED',16);
define('ACTIVITY_VIDEO_SEEKING',17);
define('ACTIVITY_VIDEO_STALLED',18);
define('ACTIVITY_VIDEO_SUSPEND',19);
define('ACTIVITY_VIDEO_TIMEUPDATE',20);
define('ACTIVITY_VIDEO_VOLUMECHANGE',21);
define('ACTIVITY_VIDEO_WAITING',22);
define('ACTIVITY_LOGIN',99);

define('PARTICIPANT_TYPE_COUNTY',1);
define('PARTICIPANT_TYPE_USER',2);