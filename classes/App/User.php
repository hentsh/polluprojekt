<?php namespace App;

use PhpOffice\PhpSpreadsheet\IOFactory;
class User
{
    static function register($email, $password, $is_admin = false)
    {


        // Hash the password
        $password = password_hash($password, PASSWORD_DEFAULT);


        // Insert user into database
        $user_id = insert('users', ['email' => $email, 'password' => $password]);


        // Return new user's ID
        return $user_id;
    }

    public static function get($county_id)
    {
        $where = $county_id ? "AND county_id=$county_id" : '';
        return get_all("
            SELECT * 
            FROM users
            LEFT JOIN counties USING (county_id)
            WHERE deleted=0 $where 
            ORDER BY name");
    }

    public static function import($filename, $filename_tmp, $county_id)
    {
        $existing_users = [];
        $ext = pathinfo($filename)['extension'];
        if ($ext != "xlsx") {
            echo "Error: Please Upload only CSV File";
        }
        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(true);
        $worksheets = $reader->listWorksheetInfo($filename_tmp);

        foreach ($worksheets as $worksheet) {

                        $sheetName = $worksheet['worksheetName'];
            $reader->setLoadSheetsOnly($sheetName);
            $spreadsheet = $reader->load($filename_tmp);
            $worksheet = $spreadsheet->getActiveSheet();
            $names = $worksheet->toArray();
            if ($names[0][0]!='Osaleja eesnimi'){
                stop(400, __('Invalid .xlsx file'));
            }
            foreach (array_slice($names, 1) as $name) {
                $name = "$name[0] $name[1]";
                if (empty($name)) {
                    continue;
                }
                if ($county_name = get_one("SELECT county_name FROM users JOIN counties USING (county_id) WHERE deleted=0 AND name = '" . addslashes($name) . "'")) {
                    $existing_users[] = "$name ($county_name)";
                } else {
                    insert('users', [
                        'county_id' => $county_id?$county_id:null,
                        'name' => $name,
                    ]);
                }
            }

            // Skip the rest of the sheets
            break;
        }
        return $existing_users;
    }

    public static function isAdmin($name)
    {
        return get_one("SELECT is_admin FROM users WHERE deleted=0 AND name = '" . addslashes($name) . "'");
    }

    public static function login($user_id)
    {
        Activity::create(ACTIVITY_LOGIN, $user_id);
        $_SESSION['user_id'] = $user_id;
    }
}