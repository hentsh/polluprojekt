<?php


namespace App;


class County
{

    public static function get($county_id = null, $json = false)
    {
        $counties = [];
        $and_county_id = $county_id ? "AND county_id=$county_id" : '';
        $fields = $json ? "CONCAT('@',county_name) label, CONCAT('@',county_id) value" : 'county_name, county_id';
        $rows = get_all("
            SELECT $fields 
            FROM counties 
                WHERE county_deleted = 0 $and_county_id 
            ORDER BY county_name");

        if ($json) {
            return json_encode($rows);
        }

        foreach ($rows as $row) {
            $counties[$row['county_id']] = $row;
        }
        return $counties;
    }

    public static function getValidIds()
    {
        $county_ids = [];
        $rows = County::get();
        foreach ($rows as $row) {
            $county_ids[] = $row['county_id'];
        }
        return $county_ids;
    }
}