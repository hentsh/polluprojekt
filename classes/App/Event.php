<?php namespace App;

class Event
{

    public static function convert($participants)
    {
        $result = [];
        foreach ($participants as $participant) {
            $result[] = [
                'label' => $participant['participant_name'],
                'value' => $participant['participant_id']
            ];
        }
        return json_encode($result);
    }

    public static function getUserEvents($auth)
    {

        $where['county_filter'] = "event_id IN (SELECT event_id FROM participants WHERE participant_type_id = " . PARTICIPANT_TYPE_USER . " AND participant_id = $auth->user_id)";
        $where['county_filter'] .= $auth->county_id ? " OR event_id IN (SELECT event_id FROM participants WHERE participant_type_id = " . PARTICIPANT_TYPE_COUNTY . " AND participant_id = $auth->county_id)" : '';
        $where['county_filter'] = "($where[county_filter])"; // WHERE (... OR ..) AND ...
        $where['event_length_filter'] = "curdate()>=event_start AND curdate()<=event_end";

        return self::get($where);
    }

    public static function get($criteria = [])
    {
        $events = [];

        // Do not return deleted events
        $criteria[] = 'event_deleted = 0';

        $where = SQL::getWhere($criteria, "event_id");

        $rows = get_all("
            SELECT
                event_id, event_name,
                date_format(event_start, '%d.%m.%Y') event_start,
                date_format(event_end, '%d.%m.%Y')   event_end,
                    'participant_id', participant_id,
                    'participant_name', participant_name
            FROM events
            LEFT JOIN (
                SELECT concat('@', participant_id)   participant_id,
                       concat('@', county_name) participant_name, event_id
                FROM participants
                         LEFT JOIN counties ON county_id = participant_id
                WHERE participants.participant_type_id = 1
                
                UNION ALL
            
                SELECT  participant_id,
                        name participant_name, event_id
                FROM participants
                         LEFT JOIN users ON user_id = participant_id
                WHERE participants.participant_type_id = 2
            
            ) participants USING(event_id)
            $where
            ORDER BY event_id, participant_name
        ");

        foreach ($rows as &$event) {
            $events[$event['event_id']]['event_name'] = $event['event_name'];
            $events[$event['event_id']]['event_start'] = $event['event_start'];
            $events[$event['event_id']]['event_end'] = $event['event_end'];

            // Make sure we have at least an empty array
            if (!isset($events[$event['event_id']]['participants'])) {
                $events[$event['event_id']]['participants'] = [];
            }

            // Add a potential participant to participants
            if ($event['participant_id']) {
                $events[$event['event_id']]['participants'][] = [
                    'participant_id' => $event['participant_id'],
                    'participant_name' => $event['participant_name']
                ];
            }
        }

        foreach ($events as $event_id => $event) {

            if (is_array($events[$event_id]['participants'])) {

                usort($events[$event_id]['participants'], function ($a, $b) {
                    return ($a['participant_name'] < $b['participant_name']) ? -1 : 1;
                });

            }
        }

        return $events;
    }

    public static function onGoing($user)
    {
        $county_id = $user['county_id'] ? $user['county_id'] : 0;
        return get_one("
            SELECT event_id
            FROM participants JOIN events USING (event_id)
            WHERE event_deleted=0
              AND curdate()>=event_start
              AND curdate()<=event_end
              AND ((participant_id=$user[user_id] AND participant_type_id=2) 
                OR (participant_id=$county_id AND participant_type_id=1))");
    }

    public static function replaceParticipants($event_id, $participants)
    {
        // Delete all existing participants
        q("DELETE FROM participants WHERE event_id = $event_id");

        if ($participants) foreach (explode(', ', $participants) as $participant_id) {
            $first_character = substr($participant_id, 0, 1);
            $participant_id = $first_character == '@' ? substr($participant_id, 1) : $participant_id;
            $participant_type_id = $first_character == '@' ? PARTICIPANT_TYPE_COUNTY : PARTICIPANT_TYPE_USER;
            insert('participants', [
                'participant_id' => $participant_id,
                'participant_type_id' => $participant_type_id,
                'event_id' => $event_id,
            ]);
        }
    }
}