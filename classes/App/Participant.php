<?php


namespace App;


class Participant
{

    public static function get()
    {
        return get_all("
            SELECT
                CONCAT('@', county_id) value, 
                CONCAT('@', county_name) label
                
            FROM counties
                     
                UNION ALL
            
            SELECT
                user_id value,
                name label
            
            FROM users WHERE deleted = 0
        ");
    }

}